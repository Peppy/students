# Students

`Students` is a minetest mod to perform various actions on a group of players.

**This mod is no longer maintained.**

**Please consider using [Classroom](https://forum.minetest.net/viewtopic.php?f=9&t=23715) mod by Rubenwardy instead.**


## Group actions

- Group grant/revoke _creative_ privilège.
- Freeze/unfreeze your students.
- Gather together your students.
- Heal your students.
- More to come...

## Credits

Most of the code comes from [Protector redo](https://github.com/tenplus1/protector) mod by TenPlus1.

Freeze code from [Freeze](https://github.com/PenguinDad/freeze) mod from PenguinDad.