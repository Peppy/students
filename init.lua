
-- get minetest.conf settings
students = {}
--protector.mod = "redo"

-- Load support for intllib.
local MP = minetest.get_modpath(minetest.get_current_modname())
local S, NS = dofile(MP.."/intllib.lua")

students.intllib = S

-- return list of members as a table
students.get_member_list = function(meta)

	return meta:get_string("members"):split(" ")
end

-- write member list table in students meta as string
students.set_member_list = function(meta, list)

	meta:set_string("members", table.concat(list, " "))
end

-- check if player name is a member
students.is_member = function (meta, name)

	for _, n in pairs(students.get_member_list(meta)) do

		if n == name then
			return true
		end
	end

	return false
end

-- add player name to table as member
students.add_member = function(meta, name)

	if students.is_member(meta, name) then
		return
	end

	local list = students.get_member_list(meta)

	table.insert(list, name)

	students.set_member_list(meta, list)
end

-- remove player name from table
students.del_member = function(meta, name)

	local list = students.get_member_list(meta)

	for i, n in pairs(list) do

		if n == name then
			table.remove(list, i)
			break
		end
	end

	students.set_member_list(meta, list)
end

-- students interface
students.generate_formspec = function(meta)

	local zonename = meta:get_string("zonename")
	local formspec = "size[10,9]"
		.. default.gui_bg
		.. default.gui_bg_img
		.. default.gui_slots
--		.. "label[2.5,0;" .. S("-- students interface --") .. "]"
--		.. "label[0,0.5;" .. S("PUNCH node to show protected area or USE for area check") .. "]"
		.. "field[0.5,0.8;9.5,0.5;zone_name;"..S("Enter zone name")..";".. zonename .."]"
		.. "label[0,1.3;" .. S("Students:") .. "]"
		.. "label[0.0,6.8;" .. S("Actions:") .. "]"
		.. "button_exit[0.0,7.5;2.0,0.5;creative;" .. S("(not)Creative") .. "]"
		.. "button_exit[2.0,7.5;2.0,0.5;freeze;" .. S("(Un)Freeze") .. "]"
		.. "button_exit[4.0,7.5;2.0,0.5;assembly;" .. S("Assembly") .. "]"
		.. "button_exit[6.0,7.5;2.0,0.5;heal;" .. S("Heal") .. "]"
		.. "button_exit[8.0,7.5;2.0,0.5;other;" .. S("Other") .. "]"
		.. "button_exit[4.0,8.5;2.0,0.5;close_me;" .. S("Close") .. "]"
	local members = students.get_member_list(meta)
	local npp = 25 -- max users added to students list
	local i = 0

	for n = 1, #members do

		if i < npp then

			-- show username
			formspec = formspec .. "button[" .. (i % 5 * 2)
			.. "," .. math.floor(i / 5 + 2)
			.. ";1.5,.5;students_member;" .. members[n] .. "]"

			-- username remove button
			.. "button[" .. (i % 5 * 2 + 1.25) .. ","
			.. math.floor(i / 5 + 2)
			.. ";.75,.5;students_del_member_" .. members[n] .. ";X]"
		end

		i = i + 1
	end

	if i < npp then

		-- user name entry field
		formspec = formspec .. "field[" .. (i % 5 * 2 + 1 / 3) .. ","
		.. (math.floor(i / 5 + 2) + 1 / 3)
		.. ";1.433,.5;students_add_member;;]"

		-- username add button
		.."button[" .. (i % 5 * 2 + 1.25) .. ","
		.. math.floor(i / 5 + 2) .. ";.75,.5;students_submit;+]"

	end

	return formspec
end

-- Infolevel:
-- 0 for no info
-- 1 for "This area is owned by <owner> !" if you can't dig
-- 2 for "This area is owned by <owner>.
-- 3 for checking protector overlaps

students.can_dig = function(r, pos, digger, onlyowner, infolevel)

	if not digger
	or not pos then
		return false
	end

	-- protection_bypass privileged users can override protection
	if minetest.check_player_privs(digger, {protection_bypass = true}) and infolevel == 1 then
		return true
	end

	-- infolevel 3 is only used to bypass priv check, change to 1 now
	if infolevel == 3 then infolevel = 1 end

	-- find the students nodes
	local pos = minetest.find_nodes_in_area(
		{x = pos.x - r, y = pos.y - r, z = pos.z - r},
		{x = pos.x + r, y = pos.y + r, z = pos.z + r},
		{"students:block"})

	local meta, owner

	for n = 1, #pos do

		meta = minetest.get_meta(pos[n])
		owner = meta:get_string("owner") or ""
--		members = meta:get_string("members") or ""

		-- node change and digger isn't owner
		if owner ~= digger
		and infolevel == 1 then

			-- and you aren't on the member list
			if onlyowner
			or not students.is_member(meta, digger) then

				minetest.chat_send_player(digger,S("This block is owned by @1! (@2)",owner,meta:get_string("zonename")))
					return false
			end
		end

	end

	return true
end

-- students node
minetest.register_node("students:block", {
	description = S("students Block"),
	drawtype = "nodebox",
	tiles = {"info_top_bot.png","info_top_bot.png","info.png","info.png","info.png","info.png"},
	sounds = default.node_sound_stone_defaults(),
	groups = {dig_immediate = 2, unbreakable = 1},
	is_ground_content = false,
	paramtype = "light",
	light_source = 4,
	on_construct = function(pos, placer)
		local meta = minetest.get_meta(pos)
		meta:set_string("zonename", S("My school"))
	end,
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5 ,-0.5, -0.5, 0.5, 0.5, 0.5},
		}
	},

	after_place_node = function(pos, placer)

		local meta = minetest.get_meta(pos)

		meta:set_string("owner", placer:get_player_name() or "")
		meta:set_string("infotext", S("students block (owned by @1)", meta:get_string("owner"))
			.. " - " .. meta:get_string("zonename"))
		meta:set_string("members", "")
		meta:set_int("creative", 0)
		meta:set_int("frozen", 0)
	end,

	on_rightclick = function(pos, node, clicker, itemstack)

		local meta = minetest.get_meta(pos)

		if meta
		and students.can_dig(1, pos,clicker:get_player_name(), true, 1) then
			minetest.show_formspec(clicker:get_player_name(),
			"students:node_" .. minetest.pos_to_string(pos), students.generate_formspec(meta))
		end
	end,

	can_dig = function(pos, player)

		return students.can_dig(1, pos, player:get_player_name(), true, 1)
	end,

	on_blast = function() end,
})

minetest.register_craft({
	output = "students:block",
	recipe = {
		{"default:stone", "default:stone", "default:stone"},
		{"default:stone", "default:steel_ingot", "default:stone"},
		{"default:stone", "default:stone", "default:stone"},
	}
})

-- check formspec buttons or when name entered
minetest.register_on_player_receive_fields(function(player, formname, fields)

	-- students formspec found
	if string.sub(formname, 0, string.len("students:node_")) == "students:node_" then

		local pos_s = string.sub(formname, string.len("students:node_") + 1)
		local pos = minetest.string_to_pos(pos_s)
		local meta = minetest.get_meta(pos)
		local owner = meta:get_string("owner")

		if fields.zone_name == nil or fields.zone_name == "" then
			return false
		else
			meta:set_string("zonename", fields.zone_name)
			meta:set_string("infotext", S("Welcome to @1 zone !" , fields.zone_name))
		end

		-- only owner can add names
		if not students.can_dig(1, pos, player:get_player_name(), true, 1) then
			return
		end

		-- add member [+]
		if fields.students_add_member then

			for _, i in pairs(fields.students_add_member:split(" ")) do
				students.add_member(meta, i)
			end
		end

		-- remove member [x]
		for field, value in pairs(fields) do

			if string.sub(field, 0,
				string.len("students_del_member_")) == "students_del_member_" then

				students.del_member(meta,
					string.sub(field,string.len("students_del_member_") + 1))
			end
		end

		-- grant or revoke creative priv
		if fields.creative then
			if meta:get_int("creative") == 0 then
				for _, n in pairs(students.get_member_list(meta)) do
					local privs = minetest.get_player_privs(n)
					privs.creative = true
					minetest.set_player_privs(n, privs)
					meta:set_int("creative", 1)
				end
				minetest.chat_send_player(owner,S("Your students are in creative mode now."))
			else
				for _, n in pairs(students.get_member_list(meta)) do
					local privs = minetest.get_player_privs(n)
					privs.creative = nil
					minetest.set_player_privs(n, privs)
					meta:set_int("creative", 0)
				end
				minetest.chat_send_player(owner,S("Your students are no more in creative mode."))
			end
		end

		-- heal players
		if fields.heal then
			for _, n in pairs(students.get_member_list(meta)) do
				if minetest.get_player_by_name(n) then
					minetest.get_player_by_name(n):set_hp(20)
				end
			end
			minetest.chat_send_player(owner,S("Your students have been healed."))
		end

		-- (un)freeze all menbers
		if fields.freeze then
--			local owner = meta:get_string("owner")
			if meta:get_int("frozen") == 0 then
				for _, n in pairs(students.get_member_list(meta)) do
					local student = minetest.get_player_by_name(n)
					if student then
						student:set_physics_override(0, 0, 1.5)
						meta:set_int("frozen", 1)
					end
				end
				minetest.chat_send_player(owner,S("Your students are now frozen!"))
			else
				for _, n in pairs(students.get_member_list(meta)) do
					local student = minetest.get_player_by_name(n)
					if student then
						student:set_physics_override(1, 1, 1)
						meta:set_int("frozen", 0)
					end
				end
				minetest.chat_send_player(owner,S("Your students are now free!"))
			end
		end

		-- teleport menbers to owner
		if fields.assembly then
--			local meta = minetest.get_meta(pos)
--			local owner = minetest.get_player_by_name(meta:get_string("owner"))
--			local pos = owner:getpos()
			for _, n in pairs(students.get_member_list(meta)) do
				local student = minetest.get_player_by_name(n)
				if student then
					student:setpos(pos)
				end
			end
		end

		-- reset formspec until close button pressed
		if not fields.close_me then
			minetest.show_formspec(player:get_player_name(), formname, students.generate_formspec(meta))
		end
	end
end)

--local path = minetest.get_modpath("students")

--dofile(path .. "/admin.lua")

print (S("[MOD] Students loaded"))
